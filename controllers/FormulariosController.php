<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Formulario;

class FormulariosController extends Controller {

    public function actionPrueba() {

        return ($this->render("formulario"));
    }

    public function actionIndex() {
        $id = Yii::$app->request->post();
        if ($id) {
            $mensaje = $id;
        } else {
            return $this->redirect(["formularios/formu1"]);
        }
        return $this->render("index", [
                    "mensaje" => $mensaje,
        ]);
    }


    public function actionFormu1() {
        return $this->render("formulario");
    }

    public function actionResultados() {
        $id = Yii::$app->request->post();
        return $this->render("resultados", [
                    "id" => $id
        ]);
    }

    public function actionFormu2() {
        $model = new Formulario();
        $dato = Yii::$app->request->post();
        if ($dato) {
            $model->load(Yii::$app->request->post());
            return $this->render("index", [
                        "mensaje" => $model
            ]);
        }
    
        return $this->render("formulario1", [
                    "model" => $model,
        ]);
    }
    
    public function actionFormu3() {
        $model = new Formulario();
        $dato = Yii::$app->request->post();
        
        if ($dato) {
            $model->load(Yii::$app->request->post());
            //if($model->validate());
            if($model->validate()){
            return $this->render("index1", [
                        "mensaje" => $model,
                        "nombreCompleto"=>$model->getNombreCompleto(),
                        "imc"=>$model->getImc(),
                        "model"=>$model
            ]);
            }
        }
        
        return $this->render("formulario3", [
                    "model" => $model,
                   
        ]);
    }
    
    


}
