<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$form = ActiveForm::begin([
    'id' => 'login-form',
    'options' => ['class' => 'form-horizontal'],
]) ;
//$var = [ 0 => 'Santander', 1 => 'Torrelavega',  2 => 'Potes'];
        
        ?>
    <?= $form->field($model, 'nombre'); ?>
    <?= $form->field($model, 'apellidos'); ?>
    <?= $form->field($model, 'poblacion')->dropDownList($model->getValoresPoblacion(), ['prompt' => 'Seleccione una población' ]); ?>
    <?= $form->field($model, 'peso'); ?>
    <?= $form->field($model, 'altura'); ?>




    <div class="form-group">
        <div class="col-lg-offset-1 col-lg-11">
            <?= Html::submitButton('Enviar', ['class' => 'btn btn-primary']) ?>
        </div>
    </div>
<?php ActiveForm::end() ?>