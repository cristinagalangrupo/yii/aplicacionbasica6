<?php 
use yii\widgets\DetailView;
?>
<?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            
            'nombre',
            'apellidos',
            'altura',
            'peso',
            'poblacion',
            ['nombreCompleto'=>function($modelo){
                 return $model->getNombreCompleto();
            }],
            ['imc'=>function($modelo){
                 return $model->getImc();
            }]
        ],
    ]) ?>