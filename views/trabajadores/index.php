<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\TrabajadoresSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Trabajadores';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="trabajadores-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php  echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Trabajadores', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            

            'id',
            'nombre',
            'apellidos',
            'fechaNacimiento',
            [
                       'attribute'=>'foto',
                       'format'=>'raw',
                        'value' => function ($datos) {
                            $url = "@web/imgs/".$datos->foto;
                            return Html::img($url, ['alt'=>'myImage','width'=>'200px']);
                        }
                        ],
            'delegacion',
            //'delegacion0.nombre',//este es más corto pero es mejor el de abajo ['attribute'...]. Buscar en bsource => GridView Column: Data From Relational Table Or Model. Esto es para hasOne, uno en concreto
            [
                'attribute'=>'nombredelegacion',
                'value'=>'delegacion0.nombre',
                ],
            [
                'attribute'=>'poblacion',
                'value'=>'delegacion0.poblacion',
                ],

            ['class' => 'yii\grid\ActionColumn',
                'template'=>'{view}{update}{delete}{delegacion}',
                'buttons'=>['delegacion'=>function($url,$model,$key){
                        return Html::a('<span class="glyphicon glyphicon-home"></span>', ['delegacion/view',"id"=>$model->delegacion]);
                }],
                        ],
        ],
    ]); ?>
    
</div>
