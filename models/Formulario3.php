<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */

class Formulario3 extends Model
{
    public $nombre;
    public $edad;
    public $poblacion;
    public $peso;
    public $altura;
  
    public function rules()
    {
        return [
            [['nombre','peso','altura'],"required", "message"=>"El campo {attribute} no puede estar vacío"],
            [['apellidos','max'=>50]],
            
            
            [['nombre','apellidos','poblacion','peso','altura'],"safe"], //carga masiva            
        ];
    }
    public function attributeLabels() {
      return[ 'nombre'=>'Nombre del trabajador',
              'apellidos'=>'Apellidos del trabajador',
            ]  ;
        
    }
} 