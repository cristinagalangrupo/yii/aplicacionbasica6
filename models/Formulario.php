<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */

class Formulario extends Model
{
    public $nombre;
    public $apellidos;
    public $edad;
   
    public $peso;
    public $altura;
    public $valoresPoblacion=[
        "santander"=>'Santander',
        "potes"=>'Potes',
        "laredo"=>'Laredo'
    ];
  
    public function rules()
    {
        return [
            [['nombre','edad','peso','altura'],"required", "message"=>"El campo {attribute} no puede estar vacío"],
            ['edad','integer',"message"=>"El campo edad debe ser numérico"],
            ['apellidos','string','max'=>50,"tooLong"=>"Máximo 50 caracteres"],
            ['poblacion','in','range'=> array_keys($this->getValoresPoblacion())],
            ['altura','number','min'=>100,'max'=>250,"tooBig"=>"Debe ser menor de 250","tooSmall"=>"Debe ser mayor de 100"],
            [['nombre','edad','apellidos','peso','altura','peso'],"safe"], //carga masiva             
        ];
    }
    public function getValoresPoblacion(){
        return [
           "poblacion"=>$this->valoresPoblacion,
                ];
    }
    public function attributeLabels() {
      return[ 'nombre'=>'Nombre del trabajador',
              'apellidos'=>'Apellidos',
              'edad'=>'Edad del trabajador',
              'poblacion'=>'Población',
              'peso'=>'Peso',
              'altura'=>'Altura',
              
            ]  ;
        
    }
    
    public function getNombreCompleto(){
        return [$this->nombre." ".$this->apellidos];
    }
    public function getImc(){
        return[
            ($this->altura)/($this->peso)
        ];
    }
}

